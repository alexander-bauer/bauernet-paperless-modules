# Paperless does not have built-in OIDC support. Instead, we use oauth2-proxy to deploy
# a namespace-local authentication proxy.
module "auth" {
  source = "git::https://gitlab.com/alexander-bauer/bauernet-authzn-modules//keycloak/oauth2-proxy-subclient?ref=v0.6.0"

  realm    = var.keycloak_realm_name
  base_url = var.auth_proxy_url
  roles = {
    "paperless-fullaccess" = {
      description = "Full, administrative access to Paperless"
    }
    "ftp-web-fullaccess" = {
      description = "Full, administrative access to the FTP WebUI"
    }
    "scanner-web-fullaccess" = {
      description = "Full, administrative access to the Scanner WebUI"
    }
  }
}
