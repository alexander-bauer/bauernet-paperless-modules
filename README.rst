##########################
BauerNet Paperless Modules
##########################

This repository is contains Terraform modules for configuring Paperless in the BauerNet
infrastructure.

.. toctree::
   :maxdepth: 1

   */README
   CHANGELOG
