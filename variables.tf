variable "paperless_fqdn" {
  type = string
}
variable "storageclass" {
  type = string
}
variable "cert_annotations" {
  type = map(string)
}

variable "ftp_fqdn" {
  type = string
}
variable "storageclass_rwx" {
  type = string
}
variable "service_sharing_key" {
  type = string
}

variable "scanner_fqdn" {
  type = string
}
variable "scanner_ip" {
  type = string
}

variable "keycloak_realm_name" {
  description = "Keycloak realm name to operate in"
  type        = string
}
variable "auth_proxy_url" {
  description = "Base url for the OAuth2-Proxy"
  type        = string
}
variable "auth_proxy_logout_url" {
  description = "URL to direct logouts to"
  type        = string
}
variable "auth_proxy_user_header" {
  description = "Name of the proxy-filled header which contains the username"
  type        = string
}
