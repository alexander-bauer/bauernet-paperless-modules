resource "kubernetes_namespace" "paperless" {
  metadata {
    name = "paperless"
  }
}

resource "helm_release" "paperless" {
  name       = "paperless"
  repository = "https://k8s-at-home.com/charts/"
  chart      = "paperless"
  namespace  = one(kubernetes_namespace.paperless.metadata).name
  version    = "9.1.3"

  values = [
    yamlencode({
      hostname = "paperless"
      image = {
        tag = "1.9.2"
      }
      env = {
        TZ                   = "America/New_York"
        PAPERLESS_TIME_ZONE  = "America/New_York"
        PAPERLESS_DATE_ORDER = "MDY"
        PAPERLESS_REDIS      = "redis://paperless-redis-master:6379"

        PAPERLESS_ENABLE_HTTP_REMOTE_USER = true
        # this format: https://docs.djangoproject.com/en/3.1/ref/request-response/#django.http.HttpRequest.META
        # like HTTP_X_USER for header X-User
        PAPERLESS_HTTP_REMOTE_USER_HEADER_NAME = "HTTP_${replace(upper(var.auth_proxy_user_header), "-", "_")}"
        PAPERLESS_LOGOUT_REDIRECT_URL          = var.auth_proxy_logout_url

        # Database
        PAPERLESS_DBENGINE = "postgresql"
        PAPERLESS_DBHOST   = local.paperless_db_host
        PAPERLESS_DBPORT   = "5432"
        PAPERLESS_DBNAME   = local.paperless_db
        PAPERLESS_DBUSER   = local.paperless_db_user
        PAPERLESS_DBPASS   = local.paperless_db_password
      }
      ingress = {
        main = {
          enabled = true
          annotations = merge(
            var.cert_annotations,
            module.auth.annotations.nginx.role.paperless-fullaccess,
            {
              "nginx.ingress.kubernetes.io/auth-response-headers" = var.auth_proxy_user_header
            },
          )
          hosts = [
            {
              host = var.paperless_fqdn
              paths = [
                {
                  path     = "/"
                  pathType = "Prefix"
                  service = {
                    name = "paperless"
                    port = 8000
                  }
                }
              ]
            }
          ]
          tls = [{ secretName = "paperless-tls", hosts = [var.paperless_fqdn] }]
        }
      }
      persistence = {
        data = {
          enabled      = true
          accessMode   = "ReadWriteOnce"
          size         = "10Gi"
          storageClass = var.storageclass
        }
        export = {
          enabled      = true
          accessMode   = "ReadWriteOnce"
          size         = "1Gi"
          storageClass = var.storageclass
        }
        media = {
          enabled      = true
          accessMode   = "ReadWriteOnce"
          size         = "10Gi"
          storageClass = var.storageclass
        }
        consume = {
          enabled       = true
          existingClaim = kubernetes_persistent_volume_claim.ftp.metadata[0].name
          subPath       = "incoming"
        }
      }
      redis = {
        enabled      = true
        architecture = "standalone"
        auth         = { enabled = false }
        master = {
          persistence = {
            enabled = false
          }
        }
        fullnameOverride = "paperless-redis"
      }
    })
  ]
}
