locals {
  paperless_db          = "paperless"
  paperless_db_user     = "paperless"
  paperless_db_password = random_password.paperless_db_password.result
  paperless_db_host = join("", [
    one(data.kubernetes_service.paperless_db.metadata).name,
    ".${one(kubernetes_namespace.paperless.metadata).name}.svc",
  ])
  paperless_db_port = "5432"
  paperless_db_url = join("", [
    "postgresql://",
    "${local.paperless_db_user}:${local.paperless_db_password}",
    "@${local.paperless_db_host}:${local.paperless_db_port}",
    "/${local.paperless_db}",
  ])
}

resource "random_password" "paperless_db_password" {
  length  = 32
  special = false
}

resource "helm_release" "paperless_db" {
  name       = "paperless-db"
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "postgresql"
  namespace  = one(kubernetes_namespace.paperless.metadata).name

  values = [
    yamlencode({
      auth = {
        enablePostgresUser = false
        postgresPassword   = ""
        database           = local.paperless_db
        username           = local.paperless_db_user
        password           = local.paperless_db_password
      }
      primary = {
        persistence = {
          size = "8Gi"
        }
      }
    })
  ]
}

data "kubernetes_service" "paperless_db" {
  depends_on = [helm_release.paperless_db]
  metadata {
    namespace = one(kubernetes_namespace.paperless.metadata).name
    name      = "paperless-db-postgresql"
  }
}
