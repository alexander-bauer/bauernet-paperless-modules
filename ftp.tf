locals {
  ftp_banner = "Scanner FTP"
  ftp_passive_ports = {
    start = 30000
    end   = 30004
  }
}

resource "kubernetes_persistent_volume_claim" "ftp" {
  metadata {
    name      = "ftp"
    namespace = one(kubernetes_namespace.paperless.metadata).name
  }
  spec {
    access_modes = ["ReadWriteMany"]
    resources {
      requests = {
        storage = "5Gi"
      }
    }
    storage_class_name = var.storageclass_rwx
  }
}

resource "kubernetes_config_map" "vsftpd_conf" {
  metadata {
    name      = "vsftpd-conf"
    namespace = one(kubernetes_namespace.paperless.metadata).name
  }

  data = {
    "vsftpd.conf" = <<-EOT
      listen=YES
      anonymous_enable=YES
      dirmessage_enable=YES
      use_localtime=YES
      connect_from_port_20=YES
      secure_chroot_dir=/var/run/vsftpd/empty
      #allow_writeable_chroot=YES
      write_enable=YES
      anon_upload_enable=YES
      anon_mkdir_write_enable=YES
      seccomp_sandbox=NO
      xferlog_std_format=NO
      log_ftp_protocol=YES
      anon_root=/var/ftp
      pasv_min_port=${local.ftp_passive_ports.start}
      pasv_max_port=${local.ftp_passive_ports.end}
      max_per_ip=2
      max_login_fails=2
      max_clients=5
      anon_max_rate=6250000
      ftpd_banner=${local.ftp_banner}

      # File readability
      file_open_mode=0666
      local_umask=0022
      anon_umask=0022
    EOT
  }
}

resource "kubernetes_deployment" "ftp" {
  metadata {
    name      = "ftp"
    namespace = one(kubernetes_namespace.paperless.metadata).name
  }
  spec {
    replicas = 1

    selector {
      match_labels = { app = "ftp" }
    }

    template {
      metadata {
        labels = { app = "ftp" }
      }
      spec {
        container {
          name  = "ftp"
          image = "mikenye/vsftpd-anon-uploads"

          # Main FTP port
          port {
            name           = "ftp"
            container_port = 21
          }

          # Passive ports
          dynamic "port" {
            for_each = range(local.ftp_passive_ports.start, local.ftp_passive_ports.end + 1)
            content {
              container_port = port.value
            }
          }

          volume_mount {
            # We set a sub path here because vsftpd refuses to run with a
            # writable root. It yields this error if so. ::
            #   OOPS: vsftpd: refusing to run with writable root inside chroot()
            mount_path = "/var/ftp/incoming"
            name       = "ftp"
            sub_path   = "incoming"
          }
          volume_mount {
            name       = "vsftpd-conf"
            mount_path = "/etc/vsftpd.conf"
            sub_path   = "vsftpd.conf"
            read_only  = true
          }
        }

        container {
          name              = "nginx"
          image             = "dceoy/nginx-autoindex:latest"
          image_pull_policy = "Always"

          port {
            name           = "http"
            container_port = 80
          }

          volume_mount {
            name       = "ftp"
            mount_path = "/var/lib/nginx/html"
            sub_path   = "incoming"
            read_only  = true
          }
        }

        volume {
          name = "ftp"
          persistent_volume_claim {
            claim_name = one(kubernetes_persistent_volume_claim.ftp.metadata).name
          }
        }
        volume {
          name = "vsftpd-conf"
          config_map {
            name = kubernetes_config_map.vsftpd_conf.metadata[0].name
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "ftp" {
  metadata {
    name      = "ftp"
    namespace = one(kubernetes_namespace.paperless.metadata).name
    annotations = var.service_sharing_key != null ? {
      "metallb.universe.tf/allow-shared-ip" : var.service_sharing_key
    } : {}
  }
  spec {
    selector = { app = "ftp" }
    port {
      name        = "ftp"
      port        = 21
      target_port = 21
    }

    # Passive ports
    dynamic "port" {
      for_each = range(local.ftp_passive_ports.start, local.ftp_passive_ports.end + 1)
      content {
        name        = "ftp-passive-${port.key}" # index
        port        = port.value
        target_port = port.value
      }
    }

    type = "LoadBalancer"
  }

  lifecycle {
    ignore_changes = [ # these are things that the LB service might manage
      spec[0].load_balancer_ip,
      metadata[0].annotations["kube-vip.io/vipHost"],
      metadata[0].labels["implementation"],
      metadata[0].labels["ipam-address"],
    ]
  }
}

resource "kubernetes_service" "ftp_web" {
  metadata {
    name      = "ftp-web"
    namespace = kubernetes_namespace.paperless.metadata[0].name
  }
  spec {
    selector = { app = "ftp" }
    port {
      name        = "ftp-web"
      port        = 80
      target_port = 80
    }
  }
}

resource "kubernetes_ingress_v1" "ftp_web" {
  metadata {
    name        = "ftp-web"
    namespace   = one(kubernetes_namespace.paperless.metadata).name
    annotations = merge(var.cert_annotations, module.auth.annotations.nginx.role.ftp-web-fullaccess)
  }
  spec {
    rule {
      host = var.ftp_fqdn
      http {
        path {
          path = "/"
          backend {
            service {
              name = kubernetes_service.ftp_web.metadata[0].name
              port {
                number = kubernetes_service.ftp_web.spec[0].port[0].port
              }
            }
          }
        }
      }
    }
    tls {
      secret_name = "ftp-tls"
      hosts       = [var.ftp_fqdn]
    }
  }
}
