#########
Changelog
#########

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog <https://keepachangelog.com/en/1.0.0/>`_,
and this project adheres to `Semantic Versioning <https://semver.org/spec/v2.0.0.html>`_.

[Unreleased]
============

[0.2.0] - 2023-07-15
====================

Changed
-------

* Changed OAuth2-Proxy usage for role-based access
* Changed Paperless FQDN variable from list ``paperless_fqdns`` to singleton
  ``paperless_fqdn``.

Removed
-------

* Partial work on Fava

Fixed
-----

* Fixed missing Terraform version declaration
* Fixed missing Terraform provider version requirements

[0.1.0] - 2023-05-02
====================

Added
-----

* Initial module migrated from BauerNet original
