resource "kubernetes_service" "scanner" {
  metadata {
    name      = "scanner"
    namespace = one(kubernetes_namespace.paperless.metadata).name
  }

  spec {
    port {
      protocol    = "TCP"
      name        = "http"
      port        = 80
      target_port = 80
    }
  }
}

resource "kubernetes_endpoints" "scanner" {
  metadata {
    name      = "scanner"
    namespace = one(kubernetes_namespace.paperless.metadata).name
  }

  subset {
    address {
      ip = var.scanner_ip
    }

    port {
      name     = "http"
      protocol = "TCP"
      port     = 80
    }
  }
}

resource "kubernetes_ingress_v1" "scanner" {
  metadata {
    name        = "scanner"
    namespace   = one(kubernetes_namespace.paperless.metadata).name
    annotations = merge(var.cert_annotations, module.auth.annotations.nginx.role.scanner-web-fullaccess)
  }
  spec {
    rule {
      host = var.scanner_fqdn
      http {
        path {
          path = "/"
          backend {
            service {
              name = one(kubernetes_service.scanner.metadata).name
              port {
                number = one(one(kubernetes_service.scanner.spec).port).port
              }
            }
          }
        }
      }
    }
  }
}
